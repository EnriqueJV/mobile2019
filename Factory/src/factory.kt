fun main( args: Array<String>) {
    var mazda = CarFactory().create(Color.Rojo,Fabric.Mazda)

    println(mazda.toString())
    println(mazda.color)

}


interface Car{
    var color:String
}
enum class Color{
    Rojo, Azul, Verde
}
enum class Fabric{
    Toyota, Mazda, Chevrolet
}
class Toyota (override var color: String): Car
class Mazda (override var color: String): Car
class Chevrolet (override var color: String): Car

class CarFactory {
    fun create(color: Color, fabric: Fabric): Car {
        var color = when (color) {
            Color.Azul -> "Azul"
            Color.Rojo -> "Rojo"
            Color.Verde -> "Verde"
        }
        return when (fabric) {
            Fabric.Chevrolet -> Chevrolet(color)
            Fabric.Mazda -> Mazda(color)
            Fabric.Toyota -> Toyota(color)
        }
    }
}