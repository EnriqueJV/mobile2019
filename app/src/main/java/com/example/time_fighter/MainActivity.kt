package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    //lateinit sirve para que los elementos no utilicen memoria hasta que se los vaya utilizar.
    private lateinit var welcomeMessageTextView: TextView
    private lateinit var welcomeSubtitleTextView: TextView
    private lateinit var changeTextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Se pasa el id de la vista
        welcomeMessageTextView = findViewById(R.id.welcome_message)
        welcomeMessageTextView = findViewById(R.id.welcome_subtitle)
        changeTextButton = findViewById(R.id.change_button)
        //OAra cambiar el texto se da clic al boton
        changeTextButton.setOnClickListener { changeMessageandSubtitle() }
    }

    private fun changeMessageandSubtitle() {
       // welcomeMessageTextView.text = getString(R.string.new_welcome_message)
        // welcomeSubtitleTextView.text = getString(R.string.new_welcome_subtitle)
        welcomeMessageTextView.text ="Hellow Universe"
        welcomeSubtitleTextView.text="Hey there "
    }
}
